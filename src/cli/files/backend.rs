
use structopt::StructOpt;
use async_trait::async_trait;

use mime::{ Mime };
use std::path::{ PathBuf };
use crate::cli::{ CollectionBackendCli };
use kb2::collections::{ Collection, CollectionItem };
use kb2::types::{ Result };
use kb2::db::{ Db, DbId };
use kb2::backends::files::{ FilesCollection, FilesItem };


pub struct FilesCollectionBackendCli {
}

impl FilesCollectionBackendCli {
    pub fn new() -> FilesCollectionBackendCli {
        FilesCollectionBackendCli {}
    }
}

#[async_trait]
impl CollectionBackendCli for FilesCollectionBackendCli {
    async fn create(&self, db: &Db, args: &Vec<std::ffi::OsString>) -> Result<Collection> {
        let opts = CreateOptions::from_iter(args);

        db.exec(move |conn| {
            let (mut coll, mut fcoll) = FilesCollection::create(conn)?;
        
            if let Some(ref name) = opts.name {
                coll.name = name.clone();
                coll.save(conn)?;
            }
            
            fcoll.path = opts.path;
            fcoll.max_text_size = opts.max_text_size;
            fcoll.max_content_size = opts.max_content_size;
            fcoll.save(conn)?;

            Ok(coll)
        }).await?
    }
    
    async fn subcommand(&self, _db: &Db, _args: &Vec<std::ffi::OsString>) -> Result<()> {
        Err(format_err!("Not implemented yet"))
    }
    
    async fn display_item(&self, db: &Db, id: DbId) -> Result<()> {
        let item = db.exec(move |conn| FilesItem::load(conn, id)).await??;
        println!("Path: {}", item.path);
        Ok(())
    }

    async fn item_path(&self, db: &Db, id: DbId) -> Result<(Option<Mime>, PathBuf)> {
        db.exec(move |conn| {
            let item = CollectionItem::load(conn, id)?;
            let fitem = FilesItem::load(conn, id)?;
            let fcoll = FilesCollection::load(conn, item.get_collection())?;
            let mime = fitem.mime.as_ref().and_then(|v| core::str::FromStr::from_str(v).ok());
            let mut path = PathBuf::from(fcoll.path);
            path.push(fitem.path);
            Ok((mime, path))
        }).await?
    }
}

#[derive(Debug, StructOpt)]
#[structopt()]
struct CreateOptions {
    /// Collection.
    #[structopt(short, long)]
    name: Option<String>,
    /// Root of the file tree.
    #[structopt(short, long)]
    path: String,
    /// Text files larger than this won't be parsed.
    #[structopt(short = "t", long)]
    max_text_size: Option<i64>,
    /// Maximum size of the content to generate.
    #[structopt(short = "c", long)]
    max_content_size: Option<i64>,
}
