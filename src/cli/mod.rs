
pub mod files;


use async_trait::async_trait;

use kb2::collections::{ Collection };
use kb2::types::{ Result };
use kb2::db::{ Db, DbId };


#[async_trait]
pub trait CollectionBackendCli {
    async fn create(&self, db: &Db, args: &Vec<std::ffi::OsString>) -> Result<Collection>;
    async fn subcommand(&self, db: &Db, args: &Vec<std::ffi::OsString>) -> Result<()>;
    async fn display_item(&self, db: &Db, id: DbId) -> Result<()>;
    async fn item_path(&self, db: &Db, id: DbId) -> Result<(Option<mime::Mime>, std::path::PathBuf)>;
}
