
#[macro_use]
extern crate failure;

mod cli;

use std::collections::HashMap;
use structopt::StructOpt;

use kb2::collections::{ Collection, CollectionItem, Tags };
use kb2::types::{ Result, DbId, Amrc };
use kb2::db::{ Db };
use kb2::backends::{ CollectionBackend };
use kb2::utils::{ mailcap };
use cli::{ CollectionBackendCli };


#[derive(Debug, StructOpt)]
struct Options {
    /// Database path.
    #[structopt(short, long, default_value="db.sqlite3")]
    db: String,
    #[structopt(subcommand)]
    cmd: Command,
}

#[derive(Debug, StructOpt)]
enum Command {
    Collection(CollectionCommand),
    Backend(BackendCommand),
    Search(SearchCommand),
    DisplayItem(DisplayItemCommand),
}

#[derive(Debug, StructOpt)]
enum CollectionCommand {
    Create(CollectionCreateCommand),
    List,
    Update(CollectionUpdateCommand),
    Delete(CollectionDeleteCommand),
}

#[derive(Debug, StructOpt)]
enum CollectionCreateCommand {
    #[structopt(external_subcommand)]
    Ext(Vec<std::ffi::OsString>),
}

#[derive(Debug, StructOpt)]
struct CollectionUpdateCommand {
    /// Id of the collection to update.
    #[structopt(short, long)]
    id: DbId,
}

#[derive(Debug, StructOpt)]
struct CollectionDeleteCommand {
    /// Id of the collection to delete.
    #[structopt(short, long)]
    id: DbId,
}

#[derive(Debug, StructOpt)]
struct DisplayItemCommand {
    /// Id of the item to display.
    #[structopt(short, long)]
    id: DbId,
    /// Use a pager (PAGER or more).
    #[structopt(short, long)]
    pager: bool,
    /// Use an editor (VISUAL or EDITOR or ed).
    #[structopt(short, long)]
    editor: bool,
    /// Use a mailcap entry.
    #[structopt(short, long)]
    mailcap: bool,
}

#[derive(Debug, StructOpt)]
enum BackendCommand {
    #[structopt(external_subcommand)]
    Ext(Vec<std::ffi::OsString>),
}

#[derive(Debug, StructOpt)]
struct SearchCommand {
    query: Vec<String>,
}

#[tokio::main]
async fn main() {
    let opt = Options::from_args();

    if let Err(err) = do_options(&opt).await {
        eprintln!("Error: {:?}", err);
        std::process::exit(1);
    }
}

struct App {
    db: Db,
    tags: Amrc<Tags>,
    backends: HashMap<String, Box<dyn CollectionBackend>>,
    clis: HashMap<String, Box<dyn CollectionBackendCli>>,
}

async fn do_options(options: &Options) -> Result<()> {
    let db = Db::new(kb2::db::connect(&options.db)?);
    
    let tags = Amrc::new(db.exec(move |conn| { Tags::load(conn) }).await??);
    
    let mut backends = HashMap::<String, Box<dyn CollectionBackend>>::new();
    backends.insert(kb2::backends::files::FILES_COLLECTION_BACKEND_CODE.to_owned(),
                    Box::new(kb2::backends::files::FilesCollectionBackend::new()));
    
    let mut clis = HashMap::<String, Box<dyn CollectionBackendCli>>::new();
    clis.insert(kb2::backends::files::FILES_COLLECTION_BACKEND_CODE.to_owned(),
                Box::new(cli::files::FilesCollectionBackendCli::new()));

    let mut app = App { db, tags, backends, clis };
    
    match &options.cmd {
        Command::Collection(cmd)
            => do_collection(&mut app, cmd).await,
        Command::Backend(cmd)
            => do_backend(&mut app, cmd).await,
        Command::Search(cmd)
            => do_search(&mut app, cmd).await,
        Command::DisplayItem(cmd)
            => do_display_item(&mut app, cmd).await,
    }
}

async fn do_collection(app: &mut App, options: &CollectionCommand) -> Result<()> {
    match &options {
        CollectionCommand::Create(cmd)
            => do_coll_create(app, cmd).await,
        CollectionCommand::List
            => do_coll_list(app).await,
        CollectionCommand::Update(cmd)
            => do_coll_update(app, cmd).await,
        CollectionCommand::Delete(cmd)
            => do_coll_delete(app, cmd).await,
    }
}

async fn do_coll_create(app: &mut App, options: &CollectionCreateCommand) -> Result<()> {
    let CollectionCreateCommand::Ext(args) = options;
    if args.len() < 1 {
        Err(format_err!("Expected a backend"))
    }
    else {
        let code = args[0].to_string_lossy();
        let cli = app.clis.get(&*code).ok_or_else(|| format_err!("Invalid backend: {}", code))?;
        let coll = cli.create(&app.db, &args).await?;
        println!("{}", coll.get_id());
        Ok(())
    }
}

async fn do_coll_list(app: &mut App) -> Result<()> {
    let colls = app.db.exec(move |conn| Collection::load_all(conn)).await??;

    for c in colls {
        let b = app.backends.get(c.get_backend()).ok_or_else(|| format_err!("Invalid backend: {}", c.get_backend()))?;
        let s = b.collection_summary(&app.db, c.get_id()).await?;
        println!("{}\t{}\t{}", c.get_id(), c.name, s);
    }

    Ok(())
}

async fn do_coll_update(app: &mut App, options: &CollectionUpdateCommand) -> Result<()> {
    let id = options.id;
    let c = app.db.exec(move |conn| Collection::load(conn, id)).await??;
    let b = app.backends.get(c.get_backend()).ok_or_else(|| format_err!("Invalid backend: {}", c.get_backend()))?;
    b.update_collection(&app.db, app.tags.clone(), options.id).await
}

async fn do_coll_delete(app: &mut App, options: &CollectionDeleteCommand) -> Result<()> {
    let id = options.id;
    let c = app.db.exec(move |conn| Collection::load(conn, id)).await??;
    let b = app.backends.get(c.get_backend()).ok_or_else(|| format_err!("Invalid backend: {}", c.get_backend()))?;
    b.delete_collection(&app.db, options.id).await
}

async fn do_display_item(app: &mut App, options: &DisplayItemCommand) -> Result<()> {
    if options.mailcap {
        display_item_mailcap(app, options.id).await
    }
    else if options.editor {
        display_item_external(app, options.id, &["VISUAL", "EDITOR"], "ed").await
    }
    else if options.pager {
        display_item_external(app, options.id, &["PAGER"], "more").await
    }
    else {
        display_item_inline(app, options.id).await
    }
}

async fn display_item_inline(app: &mut App, id: DbId) -> Result<()> {
    let i = app.db.exec(move |conn| CollectionItem::load(conn, id)).await??;
    let cid = i.get_collection();
    let c = app.db.exec(move |conn| Collection::load(conn, cid)).await??;
    let cli = app.clis.get(c.get_backend()).ok_or_else(|| format_err!("Invalid backend: {}", c.get_backend()))?;

    println!("Title: {}", i.title);
    
    print!("Tags:");
    {
        let tagsm = app.tags.lock("display_item_inline::tags")?;
        let tagsr = tagsm.borrow();
        let mut ts = i.tags.into_iter()
            .map(|t| tagsr.get_tag(t).unwrap_or_else(|| format!("{}", t)))
            .collect::<Vec<_>>();
        ts.sort_unstable();
        for t in ts {
            print!(" {}", t);
        }
    }
    println!("");

    println!("-");
    cli.display_item(&app.db, id).await?;
    println!("-");

    println!("{}", i.body);
    
    Ok(())
}

async fn display_item_external(app: &mut App, id: DbId, tools: &[&str], default: &str) -> Result<()> {
    let i = app.db.exec(move |conn| CollectionItem::load(conn, id)).await??;
    let cid = i.get_collection();
    let c = app.db.exec(move |conn| Collection::load(conn, cid)).await??;
    let cli = app.clis.get(c.get_backend()).ok_or_else(|| format_err!("Invalid backend: {}", c.get_backend()))?;
    let tool = tools.iter().fold(None, |r,i| r.or_else(|| std::env::var(i).ok())).unwrap_or_else(|| default.to_owned());
    let (_,path) = cli.item_path(&app.db, id).await?;
    let status = tokio::process::Command::new(tool).arg(path).status().await?;
    if status.success() {
        Ok(())
    }
    else {
        Err(format_err!("External command has failed."))
    }
}

async fn display_item_mailcap(app: &mut App, id: DbId) -> Result<()> {
    let i = app.db.exec(move |conn| CollectionItem::load(conn, id)).await??;
    let cid = i.get_collection();
    let c = app.db.exec(move |conn| Collection::load(conn, cid)).await??;
    let cli = app.clis.get(c.get_backend()).ok_or_else(|| format_err!("Invalid backend: {}", c.get_backend()))?;
    let (mime,path) = cli.item_path(&app.db, id).await?;
    let mime = mime.unwrap_or(mime::STAR_STAR);
    mailcap::open_with_mailcap(&mime, &path, mailcap::MailcapEntryMode::Terminal).await
}

async fn do_backend(app: &mut App, options: &BackendCommand) -> Result<()> {
    let BackendCommand::Ext(args) = options;
    if args.len() < 1 {
        Err(format_err!("Expected a backend"))
    }
    else {
        let code = args[0].to_string_lossy();
        let cli = app.clis.get(&*code).ok_or_else(|| format_err!("Invalid backend: {}", code))?;
        cli.subcommand(&app.db, &args).await
    }
}

async fn do_search(app: &mut App, options: &SearchCommand) -> Result<()> {
    let query = options.query[..].join(" ");
    let tags = app.tags.clone();
    app.db.exec(move |conn| {
        let items = kb2::search::find_items(conn, tags, None, None, &query)?;
        for item in items {
            let item = CollectionItem::load(conn, item)?;
            println!("{} {}", item.get_id(), item.title);
        }
        Ok(())
    }).await?
}
