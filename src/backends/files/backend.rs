
use async_trait::async_trait;

use crate::backends::{ CollectionBackend, CollectionItemFile };
use crate::types::{ Result, Amrc };
use crate::db::{ Db, DbId };
use crate::collections::{ Collection, CollectionItem, Tags };
use crate::backends::files::{ FilesCollection, FilesItem };


pub const FILES_COLLECTION_BACKEND_CODE : &str = "files";

pub struct FilesCollectionBackend {
}

impl FilesCollectionBackend {
    pub fn new() -> FilesCollectionBackend {
        FilesCollectionBackend {}
    }
}

#[async_trait]
impl CollectionBackend for FilesCollectionBackend {
    fn backend_description(&self) -> String {
        "Scans a folder for documents.".to_owned()
    }
    
    async fn collection_summary(&self, db: &Db, id: DbId) -> Result<String> {
        db.exec(move |conn| {
            let coll = FilesCollection::load(conn, id)?;
            Ok(coll.path)
        }).await?
    }
    
    async fn update_collection(&self, db: &Db, tags: Amrc<Tags>, id: DbId) -> Result<()> {
        crate::backends::files::update::update_collection(db, tags, id).await
    }
    
    async fn delete_collection(&self, db: &Db, id: DbId) -> Result<()> {
        db.exec(move |conn| {
            let coll = Collection::load(conn, id)?;
            coll.delete(conn)
        }).await?
    }
    
    async fn item_file(&self, db: &Db, id: DbId) -> Result<CollectionItemFile> {
        db.exec(move |conn| {
            let fitem = FilesItem::load(conn, id)?;
            let item = CollectionItem::load(conn, id)?;
            let fcoll = FilesCollection::load(conn, item.get_collection())?;
            Ok(CollectionItemFile {
                uri: fcoll.path + "/" + &fitem.path,
                mime: fitem.mime,
            })
        }).await?
    }
}
