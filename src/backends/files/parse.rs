
use std::path::{ Path };
use std::pin::{ Pin };
use std::future::{ Future };
use tokio::{ fs };
use tokio::sync::{ OnceCell };
use async_recursion::async_recursion;

use crate::types::{ Result, Either, Left, Right };
use crate::backends::files::{ FilesCollection };
use crate::backends::files::parsers::types::{ LazyMime, LazyText, ParsedFile };


struct ParserFn<'a, 'b, 'fut>
where 'a: 'fut, 'b: 'fut
{
    pub f: Box<dyn Fn(&'a FilesCollection, &'b Path, LazyMime, LazyText)
                      -> Pin<Box<dyn Future<Output = Either<(LazyMime, LazyText), ParsedFile>>
                                 + Send + 'fut>> + Send + 'static>,
}

impl<'a, 'b, 'fut> ParserFn<'a, 'b, 'fut> {
    fn new<F, R>(f: F) -> Self
    where F: Fn(&'a FilesCollection, &'b Path, LazyMime, LazyText) -> R,
          F: Send + 'static,
          R: Future<Output = Either<(LazyMime, LazyText), ParsedFile>>,
          R: Send + 'fut
    {
        ParserFn { f: Box::new(move |a,b,c,d| Box::pin(f(a,b,c,d))) }
    }
}


#[async_recursion]
pub async fn parse_file(fcoll: &FilesCollection, path: &Path)
                        -> Result<ParsedFile> {
    
    let parsers : Vec<ParserFn> = vec![
        ParserFn::new(crate::backends::files::parsers::gzip::parse_gzip),
        ParserFn::new(crate::backends::files::parsers::org::parse_org),
        ParserFn::new(crate::backends::files::parsers::doctotext::parse_doctotext),
        ParserFn::new(crate::backends::files::parsers::text::parse_text),
        ParserFn::new(crate::backends::files::parsers::any::parse_any),
    ];

    let meta = fs::metadata(path).await?;
    check_meta(fcoll, &meta)?;
    
    let mut res = None;
    let mut mime = OnceCell::new();
    let mut text = OnceCell::new();
    
    for parser in parsers {
        let pres = (parser.f)(fcoll, path, mime, text);
        match pres.await {
            Left((m, t)) => {
                mime = m;
                text = t;
            }
            Right(r) => {
                res = Some(r);
                break;
            }
        }
    }

    res.ok_or_else(|| format_err!("No suitable parser found for {:?}", path))
}

fn check_meta(fcoll: &FilesCollection, meta: &std::fs::Metadata) -> Result<()>
{
    let mut size_ok = true;
    if let Some(m) = fcoll.max_text_size {
        if m >= 0 && (m as u64) > meta.len() {
            size_ok = false;
        }
    }
    
    if !size_ok {
        Err(format_err!("File is too big"))
    }
    else {
        Ok(())
    }
}
