
use std::path::{ Path };
use std::collections::{ HashSet };
use regex::{ Regex };

use crate::types::{ Either, Left, Right };
use crate::backends::files::{ FilesCollection };
use crate::backends::files::parsers::common;
use crate::backends::files::parsers::types::{ LazyMime, LazyText, ParsedFile };


pub async fn parse_org(_fcoll: &FilesCollection,
                       path: &Path,
                       mime: LazyMime,
                       text: LazyText)
                       -> Either<(LazyMime, LazyText), ParsedFile>
{
    let mime_ref = common::get_lazy_mime(&mime, &path).await.ok();
    
    let mut is_org = false;
    if let Some(ref mime) = mime_ref {
        if "text" == mime.type_() && "plain" == mime.subtype() {
            is_org = Some("org") == path.extension().and_then(|v| v.to_str());
        }
    }

    if is_org {
        let text_val = common::consume_lazy_text(mime_ref, text, &path).await.unwrap_or("".to_owned());
        
        Right(ParsedFile {
            title: extract_title(&text_val),
            tags: extract_tags(&text_val),
            mime: "text/org-mode".parse().ok(),
            content: text_val,
        })
    }
    else {
        Left((mime, text))
    }
}

fn extract_title(text: &str) -> String {
    lazy_static! {
        static ref EXPR: Regex = Regex::new(r"(?m:^\*\s+([^*].*)\s*$)").unwrap();
    }

    // Only exactly one is a success.
    let mut title = None;
    for m in EXPR.captures_iter(text) {
        if title.is_none() {
            title = Some(m[1].to_owned());
        }
        else {
            title = None;
            break;
        }
    }

    title.unwrap_or("".to_owned())
}

fn extract_tags(text: &str) -> HashSet<String> {
    lazy_static! {
        static ref EXPR: Regex = Regex::new(r":([^\s\n:]+):").unwrap();
    }

    let mut tags = HashSet::new();
    let mut caps = EXPR.capture_locations();
    let mut offs = 0;

    while let Some(_) = EXPR.captures_read_at(&mut caps, text, offs) {
        let (s,e) = caps.get(1).unwrap();
        tags.insert(text[s..e].to_owned());
        offs = e - 1; // Rescan the closing colon.
    }

    tags
}
