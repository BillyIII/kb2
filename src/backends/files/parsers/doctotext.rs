
use std::path::{ Path };
use tokio::{ process };
use tokio::sync::{ OnceCell };

use crate::types::{ Result, Either, Left, Right };
use crate::backends::files::{ FilesCollection };
use crate::backends::files::parsers::text;
use crate::backends::files::parsers::types::{ LazyMime, LazyText, ParsedFile };


pub async fn parse_doctotext(fcoll: &FilesCollection,
                             path: &Path,
                             mime: LazyMime,
                             text: LazyText)
                             -> Either<(LazyMime, LazyText), ParsedFile>
{
    if let Ok(content) = get_text(&path).await {
        match text::parse_text(fcoll, path, mime, OnceCell::new_with(Some(Ok(content)))).await {
            Left((m,_)) => Left((m, text)),
            Right(r) => Right(r),
        }
    } else {
        Left((mime, text))
    }
}

async fn get_text(path: &Path) -> Result<String> {
    let output = process::Command::new("doctotext")
        .arg(path)
        .output()
        .await?;

    if !output.status.success() {
        Err(format_err!("`doctotext` has failed"))
    }
    else {
        let output = String::from_utf8(output.stdout)?;
        Ok(output)
    }
}
