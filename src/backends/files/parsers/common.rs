
use std::path::{ Path, PathBuf };
use mime::{ Mime };
use tokio::{ fs, process };
use std::os::unix::ffi::{ OsStrExt };
use std::ffi::{ OsStr };

use crate::types::{ Result, Error };
use crate::backends::files::parsers::types::{ LazyMime, LazyText };


pub async fn read_text(mime: Option<&Mime>, path: &Path)
                       -> Result<String> {
    let mut mime_ok = false;
    if let Some(ref mime) = mime {
        if mime.type_() == mime::TEXT && mime.subtype() == mime::PLAIN {
            mime_ok = true;
        }
    }

    if !mime_ok {
        Err(format_err!("Not a text file"))
    }
    else {
        let charset =
            mime.as_ref().unwrap().get_param("charset").map(|v| v.as_str().to_owned())
            .or_else(|| std::env::var("LANG").ok()
                     .and_then(|lang| env_lang::to_struct(&lang).ok()
                               .and_then(|lang| lang.charset.map(str::to_owned))))
            .unwrap_or_else(|| "utf-8".to_owned());

        let raw = fs::read(path).await?;
        
        if charset == "us-ascii" || charset == "utf-8" {
            String::from_utf8(raw).map_err(Error::from)
        }
        else {
            iconv::decode(&raw, &charset).map_err(Error::from)
        }
    }
}

pub async fn get_lazy_text<'m, 't, 'p, 'r>(mime: Option<&'m Mime>, text: &'t LazyText, path: &'p Path)
                                           -> std::result::Result<&'r String, &'r Error>
where 'm: 'r, 't: 'r, 'p: 'r
{
    text.get_or_init(|| read_text(mime, path)).await.as_ref()
}

pub async fn consume_lazy_text(mime: Option<&Mime>, text: LazyText, path: &Path)
                               -> Result<String> {
    let _ = get_lazy_text(mime, &text, path).await;
    text.into_inner().unwrap_or(Err(format_err!("get_lazy_text: Unexpected")))
}

pub async fn get_mime(file: &Path) -> Result<Mime> {
    let output = process::Command::new("file")
        .arg("-i")
        .arg(file)
        .output()
        .await?;

    if !output.status.success() {
        Err(format_err!("`file` has failed"))
    }
    else {
        let output = String::from_utf8(output.stdout)?;
        let line = output.lines().next().ok_or_else(|| format_err!("Bad `file` output"))?;
        let mut parts = line.split(':');
        parts.next().ok_or_else(|| format_err!("Bad `file` output"))?;
        let mime = parts.next().ok_or_else(|| format_err!("Bad `file` output"))?;
        mime.trim().parse().map_err(Error::from)
    }
}

pub async fn get_lazy_mime<'m, 'p, 'r>(mime: &'m LazyMime, path: &'p Path)
                                       -> std::result::Result<&'r Mime, &'r Error>
where 'm: 'r, 'p: 'r
{
    mime.get_or_init(|| get_mime(path)).await.as_ref()
}

pub async fn consume_lazy_mime(mime: LazyMime, path: &Path) -> Result<Mime> {
    let _ = mime.get_or_init(|| get_mime(path)).await;
    mime.into_inner().unwrap_or(Err(format_err!("get_lazy_mime: Unexpected")))
}


// None of the mktemp crates seem to do what I want. :-/
pub struct TempFile {
    path: PathBuf,
}

impl TempFile {
    pub async fn new(prefix: &str, suffix: &str) -> Result<TempFile> {
        let output = process::Command::new("mktemp")
            .arg("-t") // Technically deprecated on GNU, but is compatible with OpenBSD.
            .arg(format!("{}XXXXXXXXXX{}", prefix, suffix))
            .output()
            .await?;

        if !output.status.success() {
            Err(format_err!("`mktemp` has failed"))
        }
        else {
            if let Some(line) = OsStr::from_bytes(&output.stdout).to_str().and_then(|v| v.lines().next()) {
                Ok(TempFile {
                    path: Path::new(line).to_path_buf()
                })
            }
            else {
                Err(format_err!("`mktemp` is empty"))
            }
        }
    }

    pub fn path(&self) -> &Path {
        &self.path
    }
}

impl Drop for TempFile {
    fn drop(&mut self) {
        let _ = std::fs::remove_file(&self.path);
    }
}
