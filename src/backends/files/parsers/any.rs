
use std::path::{ Path };
use std::collections::{ HashSet };

use crate::types::{ Either, Right };
use crate::backends::files::{ FilesCollection };
use crate::backends::files::parsers::common;
use crate::backends::files::parsers::types::{ LazyMime, LazyText, ParsedFile };


pub async fn parse_any(_fcoll: &FilesCollection,
                       path: &Path,
                       mime: LazyMime,
                       text: LazyText)
                       -> Either<(LazyMime, LazyText), ParsedFile>
{
    let mime = common::consume_lazy_mime(mime, path).await.ok();
    let text = common::consume_lazy_text(mime.as_ref(), text, path).await.unwrap_or("".to_owned());
    
    Right(ParsedFile {
        mime: mime,
        title: "".to_owned(),
        content: text,
        tags: HashSet::new(),
    })
}
