
use mime::{ Mime };
use std::collections::{ HashSet };
use tokio::sync::{ OnceCell };

use crate::types::{ Result };


pub type LazyMime = OnceCell<Result<Mime>>;
pub type LazyText = OnceCell<Result<String>>;

pub struct ParsedFile {
    pub mime: Option<Mime>,
    pub title: String,
    pub content: String,
    pub tags: HashSet<String>,
}
