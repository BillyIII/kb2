
use std::path::{ Path };
use tokio::process::{ Command };
use std::process::{ Stdio };
use tokio::fs::{ File };
use tokio::io::{ AsyncWrite };
use tokio_stream::{ StreamExt };
use tokio_util::io::{ ReaderStream, poll_write_buf };
use std::future::{ poll_fn };
use std::io::{ Cursor };
use bytes::{ Buf };
use std::pin::{ Pin };
// use std::ffi::{ OsString };

use crate::types::{ Result, Either, Left, Right };
use crate::backends::files::{ FilesCollection };
use crate::backends::files::parsers::common;
use crate::backends::files::parsers::types::{ LazyMime, LazyText, ParsedFile };


pub async fn parse_gzip(fcoll: &FilesCollection,
                        path: &Path,
                        mime: LazyMime,
                        text: LazyText)
                        -> Either<(LazyMime, LazyText), ParsedFile>
{
    let mut is_gzip = false;
    if let Ok(ref mime) = common::get_lazy_mime(&mime, path).await {
        is_gzip = "application" == mime.type_() && "gzip" == mime.subtype();
    }

    if is_gzip {
        match unzip_and_parse(fcoll, path).await {
            Ok(p) => Right(p),
            Err(_) => Left((mime, text)),
        }
    }
    else {
        Left((mime, text))
    }
}

pub async fn unzip_and_parse(fcoll: &FilesCollection,
                             path: &Path)
                             -> Result<ParsedFile>
{
    let tmp = common::TempFile::new("kb2-", path.file_stem().and_then(|v| v.to_str()).unwrap_or("")).await?;
    let mut tfile = File::create(tmp.path()).await?;
    let mut zcat = Command::new("zcat").arg(path).stdout(Stdio::piped()).spawn()?;
    let output = zcat.stdout.take().ok_or_else(|| format_err!("parse_gzip: stdout"))?;
    let mut ostream = ReaderStream::new(output);
    
    while let Some(chunk) = ostream.next().await {
        let mut ocursor = Cursor::new(chunk?);
        while ocursor.has_remaining() {
            poll_fn(|cx| poll_write_buf(Pin::new(&mut tfile), cx, &mut ocursor)).await?;
        }
    }

    poll_fn(|cx| AsyncWrite::poll_flush(Pin::new(&mut tfile), cx)).await?;

    if zcat.wait().await?.success() {
        crate::backends::files::parse::parse_file(fcoll, tmp.path()).await
    }
    else {
        Err(format_err!("zcat has failed"))
    }
}
