
use std::path::{ Path };
use std::collections::{ HashSet };

use crate::types::{ Either, Left, Right };
use crate::backends::files::{ FilesCollection };
use crate::backends::files::parsers::common;
use crate::backends::files::parsers::types::{ LazyMime, LazyText, ParsedFile };


pub async fn parse_text(_fcoll: &FilesCollection,
                        path: &Path,
                        mime: LazyMime,
                        text: LazyText)
                        -> Either<(LazyMime, LazyText), ParsedFile>
{
    let mime_ref = common::get_lazy_mime(&mime, path).await.ok();
    let text_ref = common::get_lazy_text(mime_ref, &text, path).await.ok();
    
    if let Some(_) = text_ref {
        let text_val = common::consume_lazy_text(mime_ref, text, path).await.unwrap_or("".to_owned());
        let mime_val = common::consume_lazy_mime(mime, path).await.ok();

        let mut title = "";
        for line in text_val.lines() {
            title = line.trim();
            if !title.is_empty() {
                break;
            }
        }
        
        Right(ParsedFile {
            mime: mime_val,
            title: title.to_owned(),
            content: text_val,
            tags: HashSet::new(),
        })
    } else {
        Left((mime, text))
    }
}
