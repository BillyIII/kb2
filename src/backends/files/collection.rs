
use crate::types::{ Result, DbId, DbConn };
use diesel::prelude::*;
use crate::schema::files_collections::dsl as s;
use crate::schema::files_collections;
use crate::collections::{ Collection };
use crate::backends::files::{ FILES_COLLECTION_BACKEND_CODE };


#[derive(Clone, Queryable, Insertable, QueryableByName)]
#[diesel(table_name = files_collections)]
pub struct FilesCollection {
    id: DbId,
    pub path: String,
    pub max_text_size: Option<i64>,
    pub max_content_size: Option<i64>,
}

impl FilesCollection {
    pub fn create(db: &mut DbConn) -> Result<(Collection, FilesCollection)> {
        let coll = Collection::create(db, FILES_COLLECTION_BACKEND_CODE)?;
        
        diesel::insert_into(s::files_collections)
            .values((s::id.eq(coll.get_id()),
                     s::path.eq(""),
                     s::max_text_size.eq(None as Option<i64>),
                     s::max_content_size.eq(None as Option<i64>)))
            .execute(db)?;

        let fcoll = FilesCollection {
            id: coll.get_id(),
            path: "".to_owned(),
            max_text_size: None,
            max_content_size: None,
        };

        Ok((coll, fcoll))
    }

    pub fn load(db: &mut DbConn, id: DbId) -> Result<FilesCollection> {
        let (id, path, max_text_size, max_content_size)
            = s::files_collections
            .filter(s::id.eq(id))
            .first::<(DbId, String, Option<i64>, Option<i64>)>(db)?;

        Ok(FilesCollection {
            id,
            path,
            max_text_size,
            max_content_size,
        })
    }
    
    pub fn delete(&self, db: &mut DbConn) -> Result<()> {
        diesel::delete(s::files_collections.filter(s::id.eq(self.id))).execute(db)?;
        Ok(())
    }

    pub fn save(&self, db: &mut DbConn) -> Result<()> {
        diesel::update(s::files_collections.filter(s::id.eq(self.id)))
            .set((s::path.eq(&self.path),
                  s::max_text_size.eq(self.max_text_size),
                  s::max_content_size.eq(self.max_content_size)))
            .execute(db)?;
        Ok(())
    }

    pub fn get_id(&self) -> DbId {
        self.id
    }
}
