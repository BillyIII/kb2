
use std::collections::{ HashSet };
use tokio_stream::{ StreamExt };
use diesel::prelude::*;
use tokio::{ fs };
use std::path::{ PathBuf, Path, Component };
use async_recursion::{ async_recursion };
use std::convert::{ TryInto };
use tokio_stream::wrappers::{ ReadDirStream };
use futures::stream::{ FuturesUnordered };

use crate::types::{ Result, Amrc, Either, Left, Right };
use crate::db::{ Db, DbId };
use crate::collections::{ Tags };
use crate::backends::files::{ FilesCollection, FilesItem };
use crate::schema::items::dsl as si;
use crate::schema::files_items::dsl as sf;
use crate::schema::items_tags::dsl as st;
use crate::backends::files::parsers::types::{ ParsedFile };


pub async fn update_collection(db: &Db, tags: Amrc<Tags>, id: DbId) -> Result<()> {
    let fcoll = db.exec(move |conn| FilesCollection::load(conn, id)).await??;

    remove_missing(db, &fcoll).await?;

    let root = PathBuf::from(&fcoll.path);

    let mut futs = FuturesUnordered::new();
    futs.push(update_path(Either::Left(root.clone()), db, tags.clone(), &fcoll, id, &root));
    while let Some(res) = futs.next().await {
        if let Some((path, stream)) = res? {
            futs.push(update_path(Either::Left(path), db, tags.clone(), &fcoll, id, &root));
            futs.push(update_path(Either::Right(stream), db, tags.clone(), &fcoll, id, &root));
        }
    }
    
    Ok(())
}

async fn remove_missing(db: &Db, fcoll: &FilesCollection) -> Result<()> {
    let fcoll_id = fcoll.get_id();
    let ress : Vec<(DbId, String)> = db.exec(move |conn| si::items
                                             .inner_join(
                                                 sf::files_items.on(
                                                     si::id.eq(sf::id)))
                                             .filter(si::collection.eq(fcoll_id))
                                             .select((si::id, sf::path))
                                             .load(conn)).await??;

    let root = PathBuf::from(&fcoll.path);
    for (id, path) in ress {
        let mut abs = root.clone();
        abs.push(path);
        
        let ex = match fs::metadata(&abs).await {
            Err(_) => false,
            Ok(meta) => meta.is_file(),
        };
        
        if !ex {
            db.exec(move |conn| diesel::delete(si::items.filter(si::id.eq(id))).execute(conn)).await??;
        }
    }
    
    Ok(())
}

// Returns either Some(path, rest) or None.
// Takes one of the two options.
// Future has to be the same exact voldemort type in both cases, hence the split argument.
// Boxing introduces lifetime issues.
#[async_recursion]
async fn update_path(arg: Either<PathBuf, ReadDirStream>,
                     db: &Db, tags: Amrc<Tags>, fcoll: &FilesCollection,
                     cid: DbId, root: &Path)
                     -> Result<Option<(PathBuf, ReadDirStream)>> {
    match arg {
        Left(path) => {
            if let Ok(meta) = fs::metadata(&path).await {
                if meta.is_dir() {
                    let rd = fs::read_dir(path).await?;
                    let rds = tokio_stream::wrappers::ReadDirStream::new(rd);
                    process_stream(rds).await
                }
                else {
                    update_file(db, tags.clone(), fcoll, cid, root, &path, &meta).await?;
                    Ok(None)
                }
            }
            else {
                Ok(None)
            }
        },
        Right(rds) => {
            process_stream(rds).await
        },
    }
}

async fn process_stream(mut rds: ReadDirStream)
                        -> Result<Option<(PathBuf, ReadDirStream)>> {
    if let Some(next) = rds.next().await {
        let path = next?.path();
        Ok(Some((path, rds)))
    }
    else {
        Ok(None)
    }
}

async fn update_file(db: &Db, all_tags: Amrc<Tags>, fcoll: &FilesCollection,
                     cid: DbId, root: &Path, path: &PathBuf, meta: &std::fs::Metadata)
                     -> Result<()> {
    let relpath = path.strip_prefix(root)?.to_str().ok_or_else(|| format_err!("Invalid path: {:?}", path))?;
    let mtime : i64 = meta.modified()?.duration_since(std::time::SystemTime::UNIX_EPOCH)?.as_secs().try_into()?;

    let tmprelpath = relpath.to_owned();
    let id_mtime : Option<(DbId, i64)> = db.exec(move |conn| si::items
                                                 .inner_join(sf::files_items.on(si::id.eq(sf::id)))
                                                 .filter(si::collection.eq(cid).and(sf::path.eq(tmprelpath)))
                                                 .select((si::id, si::mtime))
                                                 .first(conn)).await?.optional()?;

    let isnew = match id_mtime {
        None => true,
        Some((_, oldmtime)) if oldmtime < mtime => true,
        _ => false
    };

    if isnew {
        let ParsedFile { mime, mut title, content: body, tags: mut strtags } =
            crate::backends::files::parse::parse_file(fcoll, path).await?;

        let rel = path.strip_prefix(PathBuf::from(&fcoll.path)).unwrap_or(path);

        if title.is_empty() {
            title = rel.to_str().unwrap_or("").to_owned();
        }
        
        append_path_tags(rel, &mut strtags);
        
        let mime = mime.map(|v| v.to_string());

        let idtags = db.exec(move |conn| -> Result<_> {
            let ts = all_tags.lock("update_file::all_tags")?;
            let mut idtags = HashSet::new();
            for ref t in strtags {
                idtags.insert(ts.borrow_mut().get_or_insert(conn, t)?);
            }
            Ok(idtags)
        }).await??;

        // db.transaction(|| {
        update_file_entry(db, cid, id_mtime.map(|(id,_)| id), title, body,
                          relpath.to_owned(), mtime, mime, idtags).await?;
        // })?;
    }

    Ok(())
}

fn append_path_tags(rel: &Path, tags: &mut HashSet<String>) -> () {
    for comp in rel.components() {
        let tag = match comp {
            Component::Normal(c) => Some(c),
            _ => None
        };
        
        if let Some(tag) = tag.and_then(std::ffi::OsStr::to_str) {
            tags.insert(tag.to_owned());
        }
    }
}

async fn update_file_entry(db: &Db, cid: DbId, mid: Option<DbId>,
                           title: String, body: String,
                           path: String, mtime: i64, mime: Option<String>,
                           tags: HashSet<DbId>) -> Result<()> {
    db.exec(move |conn| -> Result<()> {
        let id = match mid {
            None => {
                let (mut item, mut fitem) = FilesItem::create(conn, cid)?;
                
                item.title = title.to_owned();
                item.body = body.to_owned();
                item.mtime = mtime;
                item.save(conn)?;

                fitem.path = path.to_owned();
                fitem.mime = mime.clone();
                fitem.save(conn)?;

                item.get_id()
            }
            Some(id) => {
                diesel::update(si::items.filter(si::id.eq(id)))
                    .set((si::title.eq(title),
                          si::body.eq(body),
                          si::mtime.eq(mtime)))
                    .execute(conn)?;
                
                diesel::update(sf::files_items.filter(sf::id.eq(id)))
                    .set((sf::path.eq(path),
                          sf::mime.eq(mime)))
                    .execute(conn)?;

                id
            }
        };

        diesel::delete(st::items_tags.filter(st::item.eq(id))).execute(conn)?;

        for ref tag in tags.iter() {
            diesel::insert_into(st::items_tags)
                .values((st::item.eq(id),
                         st::tag.eq(tag)))
                .execute(conn)?;
        }
        
        Ok(())
    }).await?
}
