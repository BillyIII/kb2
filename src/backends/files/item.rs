
use crate::types::{ Result, DbId, DbConn };
use diesel::prelude::*;
use crate::schema::files_items::dsl as s;
use crate::schema::files_items;
use crate::collections::{ CollectionItem };


#[derive(Clone, Queryable, Insertable, QueryableByName)]
#[diesel(table_name = files_items)]
pub struct FilesItem {
    id: DbId,
    pub path: String,
    pub mime: Option<String>,
}

impl FilesItem {
    pub fn create(db: &mut DbConn, id: DbId) -> Result<(CollectionItem, FilesItem)> {
        let item = CollectionItem::create(db, id)?;
        
        diesel::insert_into(s::files_items)
            .values((s::id.eq(item.get_id()),
                     s::path.eq(""),
                     s::mime.eq(None as Option<String>)))
            .execute(db)?;

        let fitem = FilesItem {
            id: item.get_id(),
            path: "".to_owned(),
            mime: None,
        };

        Ok((item, fitem))
    }

    pub fn load(db: &mut DbConn, id: DbId) -> Result<FilesItem> {
        let (id, path, mime)
            = s::files_items
            .filter(s::id.eq(id))
            .first::<(DbId, String, Option<String>)>(db)?;

        Ok(FilesItem {
            id,
            path,
            mime,
        })
    }
    
    pub fn delete(&self, db: &mut DbConn) -> Result<()> {
        diesel::delete(s::files_items.filter(s::id.eq(self.id))).execute(db)?;
        Ok(())
    }

    pub fn save(&self, db: &mut DbConn) -> Result<()> {
        diesel::update(s::files_items.filter(s::id.eq(self.id)))
            .set((s::path.eq(&self.path),
                  s::mime.eq(&self.mime)))
            .execute(db)?;
        Ok(())
    }

    pub fn get_id(&self) -> DbId {
        self.id
    }
}
