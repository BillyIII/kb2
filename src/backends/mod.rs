
pub mod files;


use async_trait::async_trait;

use crate::collections::{ Tags };
use crate::types::{ Result, Amrc };
use crate::db::{ Db, DbId };


pub struct CollectionItemFile {
    pub mime: Option<String>,
    pub uri: String,
}

#[async_trait]
pub trait CollectionBackend {
    fn backend_description(&self) -> String;
    async fn collection_summary(&self, db: &Db, coll: DbId) -> Result<String>;
    async fn update_collection(&self, db: &Db, tags: Amrc<Tags>, id: DbId) -> Result<()>;
    async fn delete_collection(&self, db: &Db, id: DbId) -> Result<()>;
    async fn item_file(&self, db: &Db, id: DbId) -> Result<CollectionItemFile>;
}

// pub type CollectionBackendCode = String;
