
#![feature(async_closure)]
#![feature(iter_intersperse)]

#[macro_use]
extern crate failure;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate lazy_static;
// extern crate sqlx;


mod schema;
// mod cxx;
pub mod db;
pub mod types;
pub mod collections;
pub mod backends;
pub mod search;
pub mod utils;
