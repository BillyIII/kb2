table! {
    collections (id) {
        id -> Integer,
        backend -> Text,
        name -> Text,
    }
}

table! {
    files_collections (id) {
        id -> Integer,
        path -> Text,
        max_text_size -> Nullable<BigInt>,
        max_content_size -> Nullable<BigInt>,
    }
}

table! {
    files_items (id) {
        id -> Integer,
        path -> Text,
        mime -> Nullable<Text>,
    }
}

table! {
    items (id) {
        id -> Integer,
        collection -> Integer,
        title -> Text,
        body -> Text,
        mtime -> BigInt,
    }
}

table! {
    items_tags (item, tag) {
        item -> Integer,
        tag -> Integer,
    }
}

table! {
    tags (id) {
        id -> Integer,
        tag -> Text,
    }
}

joinable!(files_collections -> collections (id));
joinable!(files_items -> items (id));
joinable!(items -> collections (collection));
joinable!(items_tags -> items (item));
joinable!(items_tags -> tags (tag));

allow_tables_to_appear_in_same_query!(
    collections,
    files_collections,
    files_items,
    items,
    items_tags,
    tags,
);
