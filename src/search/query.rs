
use diesel::query_dsl::{ RunQueryDsl };
use diesel::deserialize::{ QueryableByName };
use diesel::result::{ QueryResult };
use diesel::sql_types;

use crate::search::types::{ Value };
use crate::types::{ DbConn, DbId, DbIdSql };


pub struct Query {
    sql: String,
    args: Vec<Value>,
}

#[derive(QueryableByName)]
pub struct SqlResult {
    #[diesel(sql_type = DbIdSql)]
    pub id: DbId,
}

impl Query {
    pub fn new(sql: String, args: Vec<Value>) -> Self {
        Query { sql, args }
    }

    pub fn load(&self, conn: &mut DbConn) -> QueryResult<Vec<SqlResult>> {
        let mut query = diesel::dsl::sql_query(self.sql.clone()).into_boxed();
        for arg in &self.args {
            query = match arg {
                Value::Id(v) => query.bind::<DbIdSql, _>(v),
                Value::Int(v) => query.bind::<sql_types::BigInt, _>(v),
                Value::Str(v) => query.bind::<sql_types::Text, _>(v),
            };
        };
        query.load::<SqlResult>(conn)
    }
}
