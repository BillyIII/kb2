
use crate::types::{ DbId };


pub enum Expr {
    And(Box<Expr>, Box<Expr>),
    Or(Box<Expr>, Box<Expr>),
    Not(Box<Expr>),
    Op(Op),
}

pub enum Op {
    False,
    Tag(String),
    Collection(DbId),
    Fts(String),
    IsSet(Field),
    Eq(Field, Value),
    Lt(Field, Value),
    Gt(Field, Value),
    Between(Field, Value, Value),
}

pub struct Field(pub String);

#[derive(Clone)]
pub enum Value {
    Id(DbId),
    Str(String),
    Int(i64),
}


pub struct Sorting {
    pub field: SortingField,
    pub dir: SortingDirection,
}

pub enum SortingField {
    Rank,
    Date,
}

pub enum SortingDirection {
    Asc,
    Desc,
}


// type Q = crate::schema::items::BoxedQuery<'static, diesel::sqlite::Sqlite>;
// pub type SqlExpr = Box<dyn diesel::BoxableExpression<crate::schema::items::table,
//                                                      crate::types::Db,
//                                                      SqlType = diesel::sql_types::Bool>>;
