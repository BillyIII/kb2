
use std::collections::{ HashMap };

use crate::collections::{ Tags };
use crate::types::{ Result, Amrc };
use crate::search::types::*;


type TagNames = HashMap<String, String>;


struct SqlChunk {
    text: String,
    args: Vec<Value>,
}

impl SqlChunk {
    fn new() -> Self {
        SqlChunk {
            text: "".to_owned(),
            args: Vec::new(),
        }
    }
    
    fn append_text(&mut self, text: &str) {
        self.text.push_str(text);
    }
    
    fn append_arg(&mut self, text: &str, arg: Value) {
        self.append_text(text);
        self.args.push(arg);
    }
    
    // fn append_args(&mut self, text: &str, mut args: Vec<Value>) {
    //     self.append_text(text);
    //     self.args.append(&mut args);
    // }

    // fn append_chunk(&mut self, other: SqlChunk) {
    //     self.append_args(&other.text, other.args);
    // }

    fn finish(self) -> (String, Vec<Value>) {
        (self.text, self.args)
    }
}


pub fn sql_from_expr(tags: Amrc<Tags>, limit: Option<i64>, sort: Option<Sorting>, expr: Box<Expr>)
                     -> Result<(String, Vec<Value>)> {
    let mut sql = SqlChunk::new();
    let tag_names = collect_tags(&expr);

    // Relies on the join order. Ideally the fts table should be a subquery.
    sql.append_text("select items_fts.rowid as id from items_fts");
    sql.append_text(" left join items on (items_fts.rowid = items.id)");
    
    append_tags_joins(&mut sql, tags, &tag_names)?;
    
    sql.append_text(" where ");
    append_expr_sql(&mut sql, &tag_names, &expr);
    
    append_sort(&mut sql, &sort);
    append_limit(&mut sql, &limit);
    
    sql.append_text(";");
    Ok(sql.finish())
}
    
fn append_tags_joins(sql: &mut SqlChunk, tags: Amrc<Tags>, tag_names: &TagNames) -> Result<()> {
    for (tag,table) in tag_names {
        sql.append_text(" left join items_tags as ");
        sql.append_text(table);
        sql.append_text(" on (");
        sql.append_text(table);
        sql.append_text(".item = items.id and ");
        sql.append_text(table);
        sql.append_text(".tag ");
        if let Some(id) = tags.lock("append_tags_joins::tags")?.borrow().get_id(tag) {
            sql.append_arg("= ?)", Value::Int(id as i64));
        }
        else {
            sql.append_text("is null)");
        }
    }
    Ok(())
}

fn collect_tags(expr: &Expr) -> TagNames {
    collect_expr_tags(TagNames::new(), expr)
}

fn collect_expr_tags(mut tags: TagNames, expr: &Expr) -> TagNames {
    match expr {
        Expr::And(a, b) => collect_expr_tags(collect_expr_tags(tags, a), b),
        Expr::Or(a, b) => collect_expr_tags(collect_expr_tags(tags, a), b),
        Expr::Not(a) => collect_expr_tags(tags, a),
        Expr::Op(Op::Tag(tag)) => {
            if !tags.contains_key(tag) {
                tags.insert(tag.to_owned(), format!("tag_{}", tags.len()));
            }
            tags
        },
        _ => tags
    }
}
    
fn append_expr_sql(sql: &mut SqlChunk, tag_names: &TagNames, expr: &Expr) {
    sql.append_text("(");
    match expr {
        Expr::And(a, b) => {
            append_expr_sql(sql, tag_names, a);
            sql.append_text(" and ");
            append_expr_sql(sql, tag_names, b);
        },
        Expr::Or(a, b) => {
            append_expr_sql(sql, tag_names, a);
            sql.append_text(" or ");
            append_expr_sql(sql, tag_names, b);
        },
        Expr::Not(a) => {
            sql.append_text("not ");
            append_expr_sql(sql, tag_names, a);
        },
        Expr::Op(op) => append_op_sql(sql, tag_names, op),
    }
    sql.append_text(")");
}

fn append_op_sql(sql: &mut SqlChunk, tag_names: &TagNames, op: &Op) {
    match op {
        Op::False => {
            sql.append_text("false");
        },
        Op::Tag(tag) => {
            // unwrap: tag not being in the names list is a fatal error.
            sql.append_text(tag_names.get(tag).unwrap());
            sql.append_text(".tag is not null"); 
        },
        Op::Collection(cid) => {
            // unwrap: tag not being in the names list is a fatal error.
            sql.append_arg("items.collection = ?", Value::Id(*cid));
        },
        Op::Fts(query) => {
            sql.append_arg(" items_fts match ?", Value::Str(query.clone()));
        },
        Op::IsSet(Field(field)) => {
            sql.append_text(field);
            sql.append_text(" is not null");
        },
        Op::Eq(Field(field), value) => {
            sql.append_text(field);
            sql.append_arg(" = ?", value.clone());
        },
        Op::Lt(Field(field), value) => {
            sql.append_text(field);
            sql.append_arg(" < ?", value.clone());
        },
        Op::Gt(Field(field), value) => {
            sql.append_text(field);
            sql.append_arg(" > ?", value.clone());
        },
        Op::Between(Field(field), left, right) => {
            sql.append_text(field);
            sql.append_arg(" between ?", left.clone());
            sql.append_arg(" and ?", right.clone());
        },
    }
}

fn append_sort(sql: &mut SqlChunk, sort: &Option<Sorting>) {
    if let Some(sort) = sort {
        sql.append_text(" order by");
        sql.append_text(
            match sort.field {
                SortingField::Rank => " items_fts.rank",
                SortingField::Date => " items.mtime",
            });
        sql.append_text(
            match sort.dir {
                SortingDirection::Asc => " asc",
                SortingDirection::Desc => " desc",
            });
    }
}

fn append_limit(sql: &mut SqlChunk, limit: &Option<i64>) {
    if let Some(limit) = limit {
        sql.append_arg(" limit ?", Value::Int(*limit));
    }
}
