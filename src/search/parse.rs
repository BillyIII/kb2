
use sexp::{ Sexp, Atom };
use std::convert::{ TryFrom };

use crate::types::{ Result, DbId };
use crate::search::types::*;


pub fn expr_from_sexp(s: &Sexp) -> Result<Box<Expr>> {
    parse_expr(s)
}

fn parse_expr(s: &Sexp) -> Result<Box<Expr>> {
    match &s {
        Sexp::Atom(Atom::S(a)) => parse_shorthand(a),
        Sexp::List(l) => match l[..] {
            [Sexp::Atom(Atom::S(ref op)), ref arg, ref args @ ..] => parse_expr_list(op, arg, args),
            [] => okexop(Op::False),
            _ => Err(format_err!("Invalid expression: {}", s))
        },
        _ => Err(format_err!("Invalid expression: {}", s))
    }
}

fn parse_expr_list(op: &str, arg: &Sexp, args: &[Sexp]) -> Result<Box<Expr>> {
    match op {
        "and" => parse_list_args(Expr::And, arg, args),
        "or" => parse_list_args(Expr::Or, arg, args),
        "not" if args.is_empty() => Ok(Box::new(Expr::Not(parse_expr(arg)?))),
        _ => okexop(parse_expr_op(op, arg, args)?)
    }
}

fn exop(op: Op) -> Box<Expr> {
    Box::new(Expr::Op(op))
}

fn okexop(op: Op) -> Result<Box<Expr>> {
    Ok(exop(op))
}

fn parse_list_args(ctor: impl Fn(Box<Expr>, Box<Expr>) -> Expr, arg: &Sexp, args: &[Sexp]) -> Result<Box<Expr>> {
    args.iter().fold(parse_expr(arg),
                     |res, arg| res.and_then(
                         |left| parse_expr(arg).map(
                             |right| Box::new(ctor(left, right)))))
}

fn parse_expr_op(op: &str, arg: &Sexp, args: &[Sexp]) -> Result<Op> {
    match (op, args) {
        ("tag", []) => Ok(Op::Tag(parse_string(arg)?.to_lowercase())),
        ("coll", []) => Ok(Op::Collection(parse_id(arg)?)),
        ("fts", [q]) => Ok(Op::Fts(parse_fts_query(q)?)),
        ("isset", []) => Ok(Op::IsSet(parse_field(arg)?)),
        ("eq", [n]) => Ok(Op::Eq(parse_field(arg)?, parse_value(n)?)),
        ("lt", [n]) => Ok(Op::Lt(parse_field(arg)?, parse_value(n)?)),
        ("gt", [n]) => Ok(Op::Gt(parse_field(arg)?, parse_value(n)?)),
        ("between", [l,r]) => Ok(Op::Between(parse_field(arg)?, parse_value(l)?,
                                             parse_value(r)?)),
        _ => Err(format_err!("Invalid operation: {}", op))
    }
}

fn parse_string(arg: &Sexp) -> Result<String> {
    match arg {
        Sexp::Atom(Atom::S(s)) => Ok(s.to_owned()),
        _ => Err(format_err!("Expected a string, got: {}", arg))
    }
}

fn parse_fts_query(arg: &Sexp) -> Result<String> {
    parse_string(arg).map(|q| fix_fts_query(&q))
}

fn fix_fts_query(q: &str) -> String {
    q.replace('\'', "\"")
}

fn parse_field(arg: &Sexp) -> Result<Field> {
    match arg {
        Sexp::Atom(Atom::S(s)) => Ok(Field(sanitize_field(s)?)),
        _ => Err(format_err!("Expected a field, got: {}", arg))
    }
}

fn sanitize_field(s: &str) -> Result<String> {
    if s.contains(|c:char| !(c.is_ascii_alphanumeric() || c == '_' || c == '.')) {
        Err(format_err!("Invalid characters in the field: {}", s))
    } else {
        Ok(s.to_owned())
    }
}

fn parse_value(arg: &Sexp) -> Result<Value> {
    match arg {
        Sexp::List(l) if l.len() >= 1 => parse_complex_value(&l[0], &l[1..]),
        Sexp::Atom(Atom::S(s)) => Ok(Value::Str(s.to_owned())),
        Sexp::Atom(Atom::I(n)) => Ok(Value::Int(*n)),
        _ => Err(format_err!("Expected a value, got: {}", arg))
    }
}

fn parse_id(arg: &Sexp) -> Result<DbId> {
    match arg {
        Sexp::Atom(Atom::I(n)) => Ok(DbId::try_from(*n)?),
        _ => Err(format_err!("Expected an id, got: {}", arg))
    }
}

fn parse_complex_value(op: &Sexp, args: &[Sexp]) -> Result<Value> {
    if let Sexp::Atom(Atom::S(ref s)) = op {
        match (s.as_str(), &args) {
            ("time", [Sexp::Atom(Atom::S(t))]) => Ok(Value::Int(chrono::DateTime::parse_from_rfc3339(t)?.timestamp())),
            ("id", [arg]) => Ok(Value::Id(parse_id(arg)?)),
            _ => Err(format_err!("Expected a value keyword, got: {}", s))
        }
    }
    else {
        Err(format_err!("Expected a value keyword, got: {}", op))
    }
}

fn parse_shorthand(a: &str) -> Result<Box<Expr>> {
    if let Some((op, arg)) = a.split_once(':') {
        match op {
            "tag" => okexop(Op::Tag(arg.to_owned())),
            _ => Err(format_err!("Invalid shorthand op: {}", op))
        }
    }
    else {
        okexop(Op::Fts(fix_fts_query(a)))
    }
}
