
mod types;
mod parse;
mod sqlgen;
mod query;

use crate::collections::{ Tags };
use crate::types::{ Result, Error, DbId, Amrc };
use crate::db::{ DbConn };
use types::{ Value };
use query::{ Query, SqlResult };

pub use types::{ Sorting, SortingField, SortingDirection };


fn sql_from_query(tags: Amrc<Tags>, limit: Option<i64>, sort: Option<Sorting>, query: &str)
                  -> Result<(String, Vec<Value>)> {
    let sexp = sexp::parse(&format!("(and (not ()) {})", query))?;
    let expr = parse::expr_from_sexp(&sexp)?;
    sqlgen::sql_from_expr(tags, limit, sort, expr)
}

pub fn find_items(db: &mut DbConn, tags: Amrc<Tags>, limit: Option<i64>, sort: Option<Sorting>, query: &str)
                  -> Result<Vec<DbId>> {
    let (sql, args) = sql_from_query(tags, limit, sort, query)?;
    Query::new(sql, args).load(db).map_err(Error::from).map(|r:Vec<SqlResult>| r.iter().map(|r| r.id).collect())
}
