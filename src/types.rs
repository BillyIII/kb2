
pub type Error = failure::Error;
pub type Result<R> = std::result::Result<R, Error>;

pub type DbId = i32;
pub type DbIdSql = diesel::sql_types::Integer;

pub type DbConn = diesel::SqliteConnection;
pub type Db = diesel::sqlite::Sqlite;

pub use futures::future::{ Either, Either::* };

pub struct Amrc<T>(pub std::sync::Arc<std::sync::Mutex<std::cell::RefCell<T>>>);

impl<T> Clone for Amrc<T> {
    fn clone(&self) -> Self {
        Amrc(self.0.clone())
    }
}

impl<T> Amrc<T> {
    pub fn new(v: T) -> Self {
        Amrc(std::sync::Arc::new(std::sync::Mutex::new(std::cell::RefCell::new(v))))
    }

    pub fn lock<'a : 'c, 'b : 'c, 'c>(&'a self, err: &'b str)
                                      -> Result<std::sync::MutexGuard<'c, std::cell::RefCell<T>>> {
        self.0.lock().map_err(|_| format_err!("{}", err))
    }
}

    
// pub struct Atmrc<T>(std::sync::Arc<std::sync::Mutex<std::cell::RefCell<T>>>);

// impl<T> Atmrc<T> {
//     pub fn new(v: T) -> Self {
//         Atmrc(std::sync::Arc::new(tokio::sync::Mutex::new(std::cell::RefCell::new(v))))
//     }

//     pub async fn lock(&self, err: &str) -> Result<std::sync::MutexGuard<'_, T> {
//         self.0.lock().map_err(|_| format_err!("{}", err))
//     }
// }


pub struct DbEntity<T> {
    pub key: DbId,
    pub value: T,
}

impl<T> DbEntity<T> {
    pub fn new(key: DbId, value: T) -> DbEntity<T> {
        DbEntity { key, value }
    }
}
