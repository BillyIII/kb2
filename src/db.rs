
use diesel::prelude::*;
// use std::sync::{ Arc, Mutex, Condvar };
// use std::thread::{ JoinHandle };
// use tokio::sync::{ Notify };
// use std::pin::{ Pin };
// use workerpool::{ Pool, thunk::{Thunk, ThunkWorker}};
use tokio::sync::{ mpsc, oneshot };
use diesel_migrations::{ EmbeddedMigrations, MigrationHarness };

use crate::types::{ Error, Result };
pub use crate::types::{ DbId, DbConn };

const DATABASE_WORKER_CHANNEL_SIZE : usize = 1;


pub mod functions {
    use diesel::sql_types::*;
    
    // no_arg_sql_function!(
    //     last_insert_rowid,
    //     Integer,
    //     "last_insert_rowid"
    // );

    sql_function! {
        fn last_insert_rowid() -> Integer;
    }

    sql_function! {
        fn substr(s: Text, start: BigInt, length: BigInt) -> Text;
    }
}

pub fn last_insert_rowid(db: &mut DbConn) -> Result<DbId> {    
    diesel::select(functions::last_insert_rowid())
        .get_result::<DbId>(db).map_err(Error::from)
}


pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!();
 
// fn run_migration(conn: &mut PgConnection) {
//     conn.run_pending_migrations(MIGRATIONS).unwrap();
// }

pub fn connect(uri: &str) -> Result<DbConn> {
    let mut db = DbConn::establish(uri)?;

    db.run_pending_migrations(MIGRATIONS).unwrap();
    // embedded_migrations::run_with_output(&db, &mut std::io::stderr())?;

    diesel::sql_query("PRAGMA foreign_keys = ON;").execute(&mut db)?;

    Ok(db)
}


type DbSendFunc = dyn FnOnce(&mut DbConn) -> () + Send + 'static;
type DbSendVal = Box<DbSendFunc>;

#[derive(Clone)]
pub struct Db {
    sender: mpsc::Sender<DbSendVal>,
}

impl Db {
    pub fn new(mut db: DbConn) -> Self {
        let (sender, mut receiver)  = mpsc::channel::<DbSendVal>(DATABASE_WORKER_CHANNEL_SIZE);

        std::thread::spawn(move || {
            while let Some(job) = receiver.blocking_recv() {
                job(&mut db);
            }
        });
        
        Db { sender }
    }

    pub async fn exec<R, F>(&self, func: F) -> Result<R>
    where R: Send + 'static,
          F: FnOnce(&mut DbConn) -> R + Send + 'static
    {
        let (rsnd, rrcv) = oneshot::channel::<R>();
        let func = Box::new(func);
        self.sender.send(Box::new(move |db| {
            rsnd.send(func(db)).unwrap_or_else(|_| panic!("Databasa::exec::rsnd"));
        })).await.map_err(|_| format_err!("Databasa::exec::sender"))?;
        rrcv.await.map_err(Error::from)
    }
}
