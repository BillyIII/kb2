
use std::collections::{ HashSet };
use diesel::prelude::*;
use crate::diesel::expression::{ AsExpression };
use crate::schema::items::dsl as s;
use crate::schema::items_tags::dsl as st;
use crate::db::functions as dbf;
// use crate::schema::items;

use crate::types::{ Result, DbId, DbConn, Error };


// #[derive(Clone, Queryable, Insertable, QueryableByName)]
// #[table_name = "items"]
#[derive(Clone)]
pub struct CollectionItem {
    id: DbId,
    collection: DbId,
    pub title: String,
    pub body: String,
    pub mtime: i64,
    pub tags: HashSet<DbId>,
}

impl CollectionItem {
    pub fn create(db: &mut DbConn, collection: DbId) -> Result<CollectionItem> {
        diesel::insert_into(s::items)
            .values((s::collection.eq(collection),
                     s::title.eq(""),
                     s::body.eq(""),
                     s::mtime.eq(0)))
            .execute(db)?;

        let id = crate::db::last_insert_rowid(db)?;
        
        Ok(CollectionItem {
            id,
            collection,
            title: "".to_owned(),
            body: "".to_owned(),
            mtime: 0,
            tags: HashSet::new(),
        })
    }

    pub fn load(db: &mut DbConn, id: DbId) -> Result<CollectionItem> {
        let (_, collection, title, body, mtime)
            = s::items
            .filter(s::id.eq(id))
            .first::<(DbId, DbId, String, String, i64)>(db)?;

        let db_tags
            = st::items_tags
            .select(st::tag)
            .filter(st::item.eq(id))
            .load::<DbId>(db)?;
        let mut tags = HashSet::new();
        for t in db_tags {
            tags.insert(t);
        }

        Ok(CollectionItem {
            id,
            collection,
            title,
            body,
            mtime,
            tags,
        })
    }

    pub fn load_preview(db: &mut DbConn, id: DbId, body_size: i64) -> Result<CollectionItem> {
        let body_size = AsExpression::<diesel::sql_types::BigInt>::as_expression(body_size);
        
        let (_, collection, title, body, mtime)
            = s::items
            .select((s::id, s::collection, s::title, dbf::substr(s::body, 0, body_size), s::mtime))
            .filter(s::id.eq(id))
            .first::<(DbId, DbId, String, String, i64)>(db)?;

        let db_tags
            = st::items_tags
            .select(st::tag)
            .filter(st::item.eq(id))
            .load::<DbId>(db)?;
        let mut tags = HashSet::new();
        for t in db_tags {
            tags.insert(t);
        }

        Ok(CollectionItem {
            id,
            collection,
            title,
            body,
            mtime,
            tags,
        })
    }

    pub fn load_body(db: &mut DbConn, id: DbId) -> Result<String> {
        s::items
            .select(s::body)
            .filter(s::id.eq(id))
            .first::<String>(db)
            .map_err(Error::from)
    }
    
    pub fn delete(&self, db: &mut DbConn) -> Result<()> {
        diesel::delete(s::items.filter(s::id.eq(self.id))).execute(db)?;
        Ok(())
    }

    pub fn get_id(&self) -> DbId {
        self.id
    }

    pub fn get_collection(&self) -> DbId {
        self.collection
    }

    pub fn save(&self, db: &mut DbConn) -> Result<()> {
        diesel::update(s::items.filter(s::id.eq(self.id)))
            .set((s::title.eq(&self.title),
                  s::body.eq(&self.body),
                  s::mtime.eq(&self.mtime)))
            .execute(db)?;

        diesel::delete(st::items_tags.filter(st::item.eq(self.id))).execute(db)?;

        for tag in self.tags.iter() {
            diesel::insert_into(st::items_tags)
                .values((st::item.eq(self.id),
                         st::tag.eq(tag)))
                .execute(db)?;
        }
        
        Ok(())
    }
}
