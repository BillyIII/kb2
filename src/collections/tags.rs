
use std::collections::{ HashMap };
use diesel::prelude::*;
use crate::schema::tags::dsl as s;

use crate::types::{ Result, DbId, DbConn };


#[derive(Clone)]
pub struct Tags {
    by_id: HashMap<DbId, String>,
    by_tag: HashMap<String, DbId>,
}


impl Tags {
    pub fn new() -> Self {
        Tags {
            by_id: HashMap::new(),
            by_tag: HashMap::new(),
        }
    }
    
    pub fn load(db: &mut DbConn) -> Result<Tags> {
        let mut by_id = HashMap::new();
        let mut by_tag = HashMap::new();

        let rows = s::tags.load::<(DbId, String)>(db)?;
        for (id, tag) in rows {
            by_id.insert(id, tag.clone());
            by_tag.insert(tag.clone(), id);
        }

        Ok(Tags {
            by_id,
            by_tag,
        })
    }

    pub fn get_id(&self, tag: &str) -> Option<DbId> {
        self.by_tag.get(&tag.to_lowercase()).map(|i| *i)
    }

    pub fn get_tag(&self, id: DbId) -> Option<String> {
        self.by_id.get(&id).map(Clone::clone)
    }

    pub fn get_or_insert(&mut self, db: &mut DbConn, tag: &str) -> Result<DbId> {
        let tag = tag.to_lowercase();
        
        if let Some(id) = self.by_tag.get(&tag) {
            Ok(*id)
        }
        else {
            diesel::insert_into(s::tags)
                .values(s::tag.eq(&tag))
                .execute(db)?;            
            let id = crate::db::last_insert_rowid(db)?;

            self.by_id.insert(id, tag.clone());
            self.by_tag.insert(tag, id);

            Ok(id)
        }
    }

    pub fn remove(&mut self, db: &mut DbConn, id: DbId) -> Result<()> {
        let tag = self.by_id.get(&id).ok_or_else(|| format_err!("Invalid id: {}", id))?;
        diesel::delete(s::tags.filter(s::id.eq(id))).execute(db)?;
        self.by_tag.remove(tag);
        self.by_id.remove(&id);
        Ok(())
    }

    pub fn iter(&self) -> std::collections::hash_map::Iter<'_, DbId, String> {
        self.by_id.iter()
    }
}
