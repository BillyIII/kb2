
use diesel::prelude::*;
use crate::schema::collections::dsl as s;
use crate::schema::collections;

use crate::types::{ Result, DbId, DbConn, Error };


#[derive(Clone, Queryable, Insertable, QueryableByName)]
#[diesel(table_name = collections)]
pub struct Collection {
    id: DbId,
    backend: String,
    pub name: String,
}

impl Collection {
    pub fn create(db: &mut DbConn, backend: &str) -> Result<Collection> {
        diesel::insert_into(s::collections)
            .values((s::backend.eq(backend),
                     s::name.eq("")))
            .execute(db)?;

        let id = crate::db::last_insert_rowid(db)?;
            
        Ok(Collection {
            id,
            backend: backend.to_owned(),
            name: "".to_owned(),
        })
    }
    
    pub fn load(db: &mut DbConn, id: DbId) -> Result<Collection> {
        let (_, backend, name)
            = s::collections.filter(s::id.eq(id)).first::<(DbId, String, String)>(db)?;

        Ok(Collection {
            id,
            backend,
            name,
        })
    }

    pub fn get_all(db: &mut DbConn) -> Result<Vec<DbId>> {
        s::collections.select(s::id).load::<DbId>(db).map_err(Error::from)
    }

    pub fn load_all(db: &mut DbConn) -> Result<Vec<Collection>> {
        let colls = s::collections.load::<(DbId, String, String)>(db)?;
        let mut res = Vec::new();

        for (id, backend, name) in colls {
            res.push(Collection {
                id,
                backend,
                name,
            });
        }

        Ok(res)
    }
    
    pub fn delete(&self, db: &mut DbConn) -> Result<()> {
        diesel::delete(s::collections.filter(s::id.eq(self.id))).execute(db)?;
        Ok(())
    }

    pub fn save(&self, db: &mut DbConn) -> Result<()> {
        diesel::update(s::collections.filter(s::id.eq(self.id)))
            .set((s::backend.eq(&self.backend),
                  s::name.eq(&self.name)))
            .execute(db)?;
        Ok(())
    }

    pub fn get_id(&self) -> DbId {
        self.id
    }

    pub fn get_backend(&self) -> &str {
        &self.backend
    }
}
