
use std::path::{ Path, PathBuf };
use mime::{ Mime };
use std::collections::{ HashMap };
use tokio::process::{ Command };

use crate::types::{ Result };


#[derive(Clone, Debug)]
pub struct MailcapEntry {
    pub mime: Mime,
    pub command: String,
    pub needs_terminal: bool,
    pub copious_output: bool,
    pub extra: HashMap<String, String>,
}


// $HOME/.mailcap:/etc/mailcap:/usr/etc/mailcap:/usr/local/etc/mailcap
pub async fn find_mailcap() -> Option<PathBuf> {
    let mut res = None;

    if let Some(mut home) = dirs::home_dir() {
        home.push(".mailcap");
        res = check_path(home).await;
    }
    
    if res == None {
        for p in ["/etc/mailcap", "/usr/etc/mailcap", "/usr/local/etc/mailcap"] {
            if res != None {
                break;
            }
            res = check_path(PathBuf::from(p)).await;
        }
    }

    res
}

async fn check_path(path: PathBuf) -> Option<PathBuf> {
    if tokio::fs::metadata(&path).await.ok()?.is_file() {
        Some(path)
    }
    else {
        None
    }
}

pub async fn parse_mailcap(path: &Path) -> Result<Vec<MailcapEntry>> {
    let bytes = tokio::fs::read(path).await?;
    let text = String::from_utf8(bytes)?;
    let mut res = Vec::new();
    
    for line in text.lines() {
        if let Some(r) = parse_entry(line) {
            res.push(r);
        }
    }

    Ok(res)
}

fn parse_entry(line: &str) -> Option<MailcapEntry>
{
    let mut it = line.split(';');
    let mime = it.next()?.trim().parse().ok()?;
    let command = it.next()?.trim().to_owned();
    let mut needs_terminal = false;
    let mut copious_output = false;
    let mut extra = HashMap::new();

    while let Some(flag) = it.next().map(str::trim) {
        if flag == "copiousoutput" {
            copious_output = true;
        }
        else if flag == "needsterminal" {
            needs_terminal = true;
        }
        else if let Some((name,value)) = flag.split_once('=') {
            extra.insert(name.trim_end().to_owned(), value.trim_start().to_owned());
        }
        else {
            extra.insert(flag.to_owned(), "".to_owned());
        }
    }

    Some(MailcapEntry { mime, command, needs_terminal, copious_output, extra })
}

pub fn mailcap_command(entry: &MailcapEntry, mime: &Mime, file: &Path) -> (String, bool)
{
    let mut res = String::new();
    let mut has_file = false;
    let mut it = entry.command.chars();
    while let Some(ch) = it.next() {
        if ch == '\\' {
            it.next();
        }
        else if ch == '%' {
            if let Some(ch) = it.next() {
                if ch == 's' {
                    res.push_str(&shell_escape(&file.to_string_lossy()));
                    has_file = true;
                }
                else if ch == 't' {
                    res.push_str(&format!("{}/{}", mime.type_(), entry.mime.subtype()));
                }
                else if ch == '{' {
                    let mut param = String::new();
                    loop {
                        if let Some(ch) = it.next() {
                            if ch == '}' {
                                if let Some(v) = mime.get_param(param.as_str()) {
                                    res.push_str(v.as_str());
                                }
                                break;
                            }
                            else {
                                param.push(ch);
                            }
                        }
                        else {
                            res.push('%');
                            res.push('{');
                            res.push_str(&param);
                            break;
                        }
                    }
                }
                else {
                    res.push('%');
                    res.push(ch);
                }
            }
            else {
                res.push(ch);
            }
        }
        else {
            res.push(ch);
        }
    }
    
    (res, has_file)
}

fn shell_escape(cmd: &str) -> String
{
    cmd.chars().intersperse('\\').collect()
}

pub async fn run_mailcap_entry(entry: &MailcapEntry, mime: &Mime, file: &Path) -> Result<()> {
    let (mut command, has_file) = mailcap_command(entry, mime, file);
    
    if !has_file {
        command.push_str(" <");
        command.push_str(&shell_escape(&file.to_string_lossy()));
    }

    if entry.copious_output {
        // TODO: Choose a pager based on environment.
        command.push_str(" | more");
    }

    let status = Command::new("sh").arg("-c").arg(command).status().await?;
    if status.success() {
        Ok(())
    }
    else {
        Err(format_err!("Mailcap command has failed"))
    }
}

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum MailcapEntryMode {
    Gui,
    Terminal,
}

pub fn choose_mailcap_entry<'ent, 'm>(entries: &'ent Vec<MailcapEntry>, mime: &'m Mime, mode: MailcapEntryMode)
                                      -> Option<&'ent MailcapEntry> {
    entries.iter().filter(|e| test_mime(e, mime) && test_mode(e, mode)).next()
}

fn test_mime(entry: &MailcapEntry, mime: &Mime) -> bool
{
    if entry.mime.type_() == mime::STAR || mime.type_() == mime::STAR {
        true
    }
    else {
        if entry.mime.type_() != mime.type_() {
            false
        }
        else {
            if entry.mime.subtype() == mime::STAR || mime.subtype() == mime::STAR {
                true
            }
            else {
                entry.mime.subtype() == mime.subtype()
            }
        }
    }
}

fn test_mode(entry: &MailcapEntry, mode: MailcapEntryMode) -> bool
{
    match mode {
        MailcapEntryMode::Gui => !(entry.copious_output || entry.needs_terminal),
        MailcapEntryMode::Terminal => entry.copious_output || entry.needs_terminal,
    }
}

pub async fn open_with_mailcap(mime: &Mime, file: &Path, mode: MailcapEntryMode) -> Result<()>
{
    let mailcap = find_mailcap().await.ok_or_else(|| format_err!("Mailcap not found"))?;
    let entries = parse_mailcap(&mailcap).await?;
    let entry = choose_mailcap_entry(&entries, mime, mode).ok_or_else(|| format_err!("No matching mailcap entry"))?;
    run_mailcap_entry(entry, mime, file).await
}
