
create table files_collections(
       id integer not null primary key references collections(id) on delete cascade,
       path text not null,
       max_text_size bigint,
       max_content_size bigint
);

create table files_items(
       id integer not null primary key references items(id) on delete cascade,
       path text not null,
       mime text
);
