
create table collections(
       id integer not null primary key,
       backend text not null,
       name text not null
);

create table items(
       id integer not null primary key,
       collection integer not null references collections(id) on delete cascade,
       title text not null,
       body text not null,
       mtime bigint not null
);

create table tags(
       id integer not null primary key,
       tag text unique not null
);

create table items_tags(
       item integer not null references items(id) on delete cascade,
       tag integer not null references tags(id) on delete cascade,
       primary key (item, tag)
);
