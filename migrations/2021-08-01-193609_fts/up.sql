
create virtual table items_fts using fts5(title, body, content='items', content_rowid='id');

create trigger items_ai after insert on items begin
  insert into items_fts(rowid, title, body) values (new.id, new.title, new.body);
end;

create trigger items_ad after delete on items begin
  insert into items_fts(items_fts, rowid, title, body) values('delete', old.id, old.title, old.body);
end;

create trigger items_au after update on items begin
  insert into items_fts(items_fts, rowid, title, body) values('delete', old.id, old.title, old.body);
  insert into items_fts(rowid, title, body) values (new.id, new.title, new.body);
end;
